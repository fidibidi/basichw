<?php
class Response {

  private $responseText;

  public function __construct() {
  }

  public function set_response_text($responseText){
    $this->responseText = $responseText;
  }

  public function send(){
    echo $this->responseText;
  }
}
?>
