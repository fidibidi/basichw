<?php
class Request {
  private $request;
  private $formattedRequest = array();

  public function __construct() {
  }

  public function set_request($HTTPRequest) {
    $this->request = $HTTPRequest;
  }

  public function value_at_index($key){
    return $this->request[$key];
  }

  //return a Request with extraneous fields removed
  public function format_request(){
    $this->formattedRequest = array();
    $this->formattedRequest['REQUEST_URI'] = $this->request['REQUEST_URI'];

    if (!empty($_POST)){
      $this->formattedRequest['POST'] = $_POST;
    }

    if (!empty($_GET)){
      $this->formattedRequest['GET'] = $_GET;
    }

    return $this->formattedRequest;
  }

}
?>
