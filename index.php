<?php

// request manager
// echo "Server Var: ";
// echo "<pre>";
// print_r($_SERVER);
// echo "</pre>";

include("core/Request.php");
include("core/Response.php");
include("App.php");

$requestHandler = new Request();
$requestHandler->set_request($_SERVER);

$responseHandler = new Response();

$hwApp = new App($requestHandler, $responseHandler);
$hwApp->generate_response();

$responseHandler->send();

?>
