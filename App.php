<?php
class App {

  private $responseHandler;
  private $requestHandler;

  public function __construct($requestHandler, $responseHandler) {
    $this->requestHandler = $requestHandler;
    $this->responseHandler = $responseHandler;
  }

  public function generate_response(){

    $requestedURI = $this->requestHandler->value_at_index("REQUEST_URI");
    if ( $requestedURI == "/basicHW/hello") {
      $responseText = "HelloView";
    } else if ($requestedURI == "/basicHW/goodbye"){
      $responseText = "GoodbyeView";
    } else if ($requestedURI == "/basicHW/"){
      $responseText = "Root View";
    } else {
      $responseText = "Unspecified View";
    }
    $this->responseHandler->set_response_text($responseText);
  }

}




 ?>
